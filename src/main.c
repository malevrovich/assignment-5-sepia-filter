#include "image.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <stdint.h>

#include "image_tools.h"
#include "io/image_io.h"
#include "transformations/sepia.h"
#include "transformations/rotate.h"
#include "transformations/fast_sepia.h"


int main( int argc, char** argv ) {
    if(argc == 1) printf("Please, choose the source file");
    else if(argc == 2) printf("Please, choose the destionation file");
    else if(argc != 3) printf("Too many arguments!");
    else { 
        struct image src_img;
        
        const enum read_status read_status = read_image_from(argv[1], &src_img);
        if(read_status != 0) {
            fprintf(stderr, "%s\n", read_status_to_str(read_status));
            return read_status;
        }
        
        struct image res = fast_sepia(src_img);
        free_image(&src_img);

        const enum write_status write_status = write_image_to(argv[2], &res);
        free_image(&res);
        
        if(write_status != 0) {
            fprintf(stderr, "%s\n", write_status_to_str(write_status));
            return write_status;
        }

        return 0;
    }
    return 1;
}
