#include "transformations/sepia.h"

#include <stdlib.h>

#include "image.h"
#include "image_tools.h"

static const uint8_t MAX_PX_VAL = 255;

uint8_t cut(uint64_t res) {
    if(res > MAX_PX_VAL) return MAX_PX_VAL; else return res;
}

void sepia_one(struct pixel* restrict px) {
    const float c[3][3] = {
        { .393f, .769f, .189f },
        { .349f, .686f, .168f },
        { .272f, .543f, .131f }
    };

    uint8_t r = px->r;
    uint8_t g = px->g;    
    uint8_t b = px->b;

    px->r = cut(r * c[0][0] + g * c[0][1] + b * c[0][2]);
    px->g = cut(r * c[1][0] + g * c[1][1] + b * c[1][2]);
    px->b = cut(r * c[2][0] + g * c[2][1] + b * c[2][2]);
}

struct image sepia( const struct image src ) {
    struct image res;
    
    copy_image(&src, &res);

    for(size_t i = 0; i < res.height * res.width; i++) sepia_one(res.data + i);

    return res;
}