#include "transformations/fast_sepia.h"

#include "image.h"
#include "image_tools.h"
#include "transformations/sepia.h"

#include <stddef.h>

extern void sse_sepia_chunk(struct pixel *);

struct image fast_sepia( const struct image src ) {
    struct image res;
    
    copy_image(&src, &res);
    
    size_t size = res.height * res.width;
    size_t idx = 0;
    for(idx = 0; idx < size; idx += 4) {
        sse_sepia_chunk(res.data + idx);
    }

    if(idx == size) return res;

    idx -= 4;
    for(;idx < size; idx++) {
        sepia_one(res.data + idx);
    }

    return res;
}