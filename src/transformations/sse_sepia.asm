section .text
global sse_sepia_chunk

; 1 1 1 2   2 2 3 3   3 4 4 4
; 1 2 3 1 | 2 3 1 2 | 3 1 2 3

section .rodata
align 16
red_coef: dd 0.272, 0.349, 0.393, 0.272
align 16
green_coef: dd 0.543, 0.686, 0.769, 0.543
align 16
blue_coef: dd 0.131, 0.168, 0.189, 0.131
align 16
max_vals: dd 255, 255, 255, 255

section .text
%macro load_coefs_to_xmms 0
    movaps xmm4, [blue_coef]
    movaps xmm5, [green_coef]
    movaps xmm6, [red_coef]
    movaps xmm7, [max_vals]
%endmacro

; Firstly coefs looks like [1, 2, 3, 1] <-- It's the indices of column in matrix
; Every iteration we need to shift this array: 
; [1, 2, 3, 1] ==> [2, 3, 1, 2] ==> [3, 1, 2, 3] ==> [1, 2, 3, 1]
%macro shift_coefs 0
    shufps xmm4, xmm4, 0b_01_00_10_01
    shufps xmm5, xmm5, 0b_01_00_10_01
    shufps xmm6, xmm6, 0b_01_00_10_01
%endmacro

%macro put_bytes_as_floats 5
    pxor %1, %1

    pinsrb %1, [rdi+%2], 0
    pinsrb %1, [rdi+%3], 4
    pinsrb %1, [rdi+%4], 8
    pinsrb %1, [rdi+%5], 12

    cvtdq2ps %1, %1
%endmacro

%macro put_pixels_to_xmms 4
    put_bytes_as_floats xmm0, %1, %2, %3, %4
    inc rdi
    put_bytes_as_floats xmm1, %1, %2, %3, %4
    inc rdi
    put_bytes_as_floats xmm2, %1, %2, %3, %4
    inc rdi
%endmacro

%macro calc 0
    mulps xmm0, xmm4
    mulps xmm1, xmm5
    mulps xmm2, xmm6

    addps xmm1, xmm2
    addps xmm0, xmm1

    cvtps2dq xmm0, xmm0

    pminud xmm0, xmm7
    movaps xmm8, xmm0
%endmacro

%macro put_res 0
    pextrb [rsi], xmm8, 0
    inc rsi
    pextrb [rsi], xmm8, 4
    inc rsi
    pextrb [rsi], xmm8, 8
    inc rsi
    pextrb [rsi], xmm8, 12
    inc rsi
%endmacro
; rdi = pixels
; b1 g1 r1 b2 | g2 r2 b3 g3 | r3 b4 g4 r4 | b5 g5 r5 b6
sse_sepia_chunk:
    mov rsi, rdi
    
    load_coefs_to_xmms

    put_pixels_to_xmms 0, 0, 0, 3
    
    calc

    put_pixels_to_xmms 0, 0, 3, 3

    put_res

    shift:
        shift_coefs
    calc

    put_pixels_to_xmms 0, 3, 3, 3

    put_res

    shift_coefs
    calc
    
    put_res


