#include "io/image_io_read.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "image.h"
#include "io/types/bmp.h"
#include "io/types/image_type.h"

char * read_status_to_str(const enum read_status status) {
    char * READ_STATUS_ERRS[12] = {
        "Succesful read", 
        "Unsupported image type!", 
        "Unable to define image type!",
        "Reading has failed during parsing file",
        "ERROR! File pointer equals to NULL",
        "Unable to open file: no access",
        "Unable to open file: path equals to NULL",
        "Unable to open file: bad file descriptor",
        "Unable to open file: file too big",
        "Unable to open file: file is dir",
        "Unable to open file: file not found",
        "Read failed"
    };
    if(status >= 12) return "Read error!";
    return READ_STATUS_ERRS[status];
}

enum read_status read_image(FILE * const in, const enum image_type type, struct image * const img) {
    if(!in) return READ_NULL_FILE_POINTER;
    
    switch (type)
    {
    case BMP: {
        const enum bmp_read_status status = from_bmp(in, img);
        if(status != 0) {
            fprintf(stderr, "%s\n", bmp_read_status_to_str(status));
            return READ_WRONG_CONTENT;
        }
        return READ_OK;
    }
    default:
        return READ_UNSUPPORTED_TYPE;
    }
}

enum read_status errno_to_read_status(const int error_number) {
    switch (error_number)
    {
        case EACCES:
            return READ_NO_ACCESS;
        case EBADF:
            return READ_BAD_DESCRIPTOR;
        case EFBIG:
            return READ_FILE_BIG;
        case EISDIR:
            return READ_IS_DIR;
        case ENOENT:
            return READ_FILE_NOT_FOUND;
        default:
            return READ_FILE_ERROR;
    }
}

enum read_status read_explicit_image_from(const char * const path,
                                             const enum image_type type,
                                             struct image* const img) {
    if(!path) return READ_NULL_PATH;
    
    FILE *inp = fopen(path, "rb");
    if(!inp) return errno_to_read_status(errno);
    
    enum read_status res = read_image(inp, type, img);

    fclose(inp);
    return res;
}

enum read_status read_image_from(const char * const path, struct image* const img) {
    if(!path) return READ_NULL_PATH;

    char *last_dot = strrchr(path, '.');
    if(!last_dot) return READ_UNDEFINED_TYPE;

    enum image_type type;
    if(ext_to_type(last_dot, &type) != 0) return READ_UNSUPPORTED_TYPE;

    return read_explicit_image_from(path, type, img);
}
