#include "io/types/image_type.h"

#include <string.h>

int ext_to_type(const char * const ext, enum image_type* const dst) {
    if(!strcmp(ext, ".bmp")) *dst = BMP;
    else return 1;
    return 0;
}
