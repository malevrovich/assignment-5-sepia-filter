#include "io/types/bmp.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "image.h"

/* Supported only BMP format with BitCount = 24 bits */

char * bmp_write_status_to_str(const enum bmp_write_status status) {
    char * BMP_WRITE_STATUS_ERRS[4] = {
        "Succesful write",
        "BMP Write error: dst is NULL",
        "BMP Write error: image to big",
        "BMP Write error: IO error"
    };
    if(status >= 4) return "BMP Read error";
    return BMP_WRITE_STATUS_ERRS[status];
}

char * bmp_read_status_to_str(const enum bmp_read_status status){
    char * BMP_READ_STATUS_ERRS[7] = {
        "Succesful bmp read",
        "BMP Read error: invalid signature",
        "BMP Read error: unsupported type of bmp",
        "BMP Read error: dst is NULL",
        "BMP Read error: invalid header",
        "BMP Read error: invalid bits",
        "BMP Read error: memory exceed"
    };
    if(status >= 7) return "BMP Read error";
    return BMP_READ_STATUS_ERRS[status];
}

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bfOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};
#pragma pack(pop)

static inline uint8_t calc_padding(uint32_t width) {
    return (4 - width * sizeof(struct pixel) % 4) % 4;
}

enum bmp_read_status from_bmp( FILE * const in, struct image * const img ) {
    if(!img) return BMP_READ_NULL_IMAGE;
    struct bmp_header header;
    
    if(fread(&header, sizeof(struct bmp_header), 1, in) != 1) 
        return BMP_READ_INVALID_HEADER;

    if(header.bfType != 0x4D42 && header.bfType != 0x424D)
        return BMP_READ_INVALID_SIGNATURE;

    if(header.biBitCount != 24)
        return BMP_READ_UNSUPPORTED_TYPE;

    if(fseek(in, (long) header.bfOffBits, SEEK_SET) != 0)
        return BMP_READ_INVALID_BITS;
    
    img->width = header.biWidth;
    img->height = header.biHeight;

    img->data = (struct pixel*) malloc(sizeof(struct pixel) * header.biWidth * header.biHeight);
    if(!img->data) {
        return BMP_READ_MEMORY_EXCEED;
    }

    uint8_t padding = calc_padding(header.biWidth);
    
    for(uint32_t i = 0; i < header.biHeight; i++) {
        if(fread(img->data + i * header.biWidth, sizeof(struct pixel), header.biWidth, in) != header.biWidth) {
            free(img->data);
            return BMP_READ_INVALID_BITS;
        }
        if(fseek(in, padding, SEEK_CUR) != 0) {
             return BMP_READ_INVALID_BITS;    
        }
    }

    return BMP_READ_OK;
}

enum bmp_write_status to_bmp( FILE * const out, const struct image * const img ) {
    if(!img) return BMP_WRITE_NULL_IMAGE;
    struct bmp_header header;

    uint32_t endian = 1;
    if(*(char *)&endian == 1) {
        /* Little endian */
        header.bfType = 0x4D42;
    } else {
        /* Big endian */
        header.bfType = 0x424D;
    }

    uint8_t padding = calc_padding(img->width);

    size_t size = sizeof(struct bmp_header) 
            + (size_t) img->width * (size_t) img->height * sizeof(struct pixel)
            + padding * (size_t) img->height;

    if(size > UINT32_MAX) {
        return BMP_WRITE_TOO_BIG;
    }

    header.bfileSize = (uint32_t) size;

    header.bfReserved = 0;
    header.bfOffBits = sizeof(struct bmp_header); 
    header.biSize = 40;
    
    if(img->width > UINT32_MAX) return BMP_WRITE_TOO_BIG;
    header.biWidth = (uint32_t) img->width;

    if(img->height > UINT32_MAX) return BMP_WRITE_TOO_BIG;
    header.biHeight = (uint32_t) img->height;
    
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biSizeImage = (uint32_t) (size - sizeof(struct bmp_header));

    /* Disabled */
    header.biCompression = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    /* Undefined */
    header.biXPelsPerMeter = 0; 
    header.biYPelsPerMeter = 0;
    
    if(fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return BMP_WRITE_IO_ERROR;

    for(int i = 0; i < img->height; i++) {
        if(fwrite(img->data + i * img->width, sizeof(struct pixel), (size_t) img->width, out) != img->width){
            return BMP_WRITE_IO_ERROR;
        }
        if(fseek(out, padding, SEEK_CUR) != 0) return BMP_WRITE_IO_ERROR;
    }

    return BMP_WRITE_OK;
}
