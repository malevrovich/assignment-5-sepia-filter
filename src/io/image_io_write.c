#include "io/image_io_write.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "image.h"
#include "io/types/bmp.h"
#include "io/types/image_type.h"

char * write_status_to_str(const enum write_status status) {
    char * WRITE_STATUS_ERRS[12] = {
        "Succesfully write", 
        "Unsupported image type!", 
        "Unable to define image type!",
        "Writing has failed during parsing file",
        "ERROR! File pointer equals to NULL",
        "Unable to open file: no access",
        "Unable to open file: path equals to NULL",
        "Unable to open file: bad file descriptor",
        "Unable to open file: file too big",
        "Unable to open file: file is dir",
        "Unable to open file: file not found",
        "Write failed"
    };
    if(status >= 12) return "Write error!";
    return WRITE_STATUS_ERRS[status];   
}

enum write_status write_image(FILE * const out, const enum image_type type, const struct image* const img) {
    if(!out) return WRITE_NULL_FILE_POINTER;
    
    switch (type)
    {
    case BMP: {
        const enum bmp_write_status status = to_bmp(out, img);
        if(status != BMP_WRITE_OK) {
            fprintf(stderr, "%s\n", bmp_write_status_to_str(status));
            return WRITE_CONTENT_WRONG;
        }
        return WRITE_OK;
    }
    default:
        return WRITE_UNSUPPORTED_TYPE;
    }
}

enum write_status errno_to_write_status(const int error_number) {
    switch (error_number)
    {
        case EACCES:
            return WRITE_NO_ACCESS;
        case EBADF:
            return WRITE_BAD_DESCRIPTOR;
        case EISDIR:
            return WRITE_IS_DIR;
        default:
            return WRITE_FILE_ERROR;
    }
}

enum write_status write_explicit_image_to(char const* const path,
                                             const enum image_type type, const struct image* const img) {
    if(!path) return WRITE_NULL_PATH;

    FILE *out = fopen(path, "wb+");
    if(!out) return errno_to_write_status(errno);

    enum write_status res = write_image(out, type, img);

    fclose(out);
    return res;
}

enum write_status write_image_to(char const* const path, const struct image* const img) {
    if(!path) return WRITE_NULL_PATH;

    char *last_dot = strrchr(path, '.');
    if(!last_dot) return WRITE_UNDEFINED_TYPE;

    enum image_type type;
    if(ext_to_type(last_dot, &type) != 0) return WRITE_UNSUPPORTED_TYPE;

    return write_explicit_image_to(path, type, img);
}
