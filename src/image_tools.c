#include "image_tools.h"

#include "image.h"

#include <inttypes.h>
#include <stdlib.h>

void copy_image(struct image const * const src, struct image *dst){
    create_image(dst, src->width, src->height);

    if(!dst->data) return;

    for(size_t i = 0; i < dst->width * dst->height; i++) {
        dst->data[i] = src->data[i];
    }
}

void free_image(struct image *img) {
    free(img->data);
}

void create_image(struct image *dst, uint64_t width, uint64_t height){
    dst->height = height;
    dst->width = width;
    dst->data = (struct pixel *) malloc(dst->width * dst->height * sizeof(struct pixel));

    if(!dst->data) {
        dst->height = 0;
        dst->width = 0;
    }
}
