PROG_NAME=prog

CFLAGS = -c -O2 -Wall -Werror -std=c17 -Wdiscarded-qualifiers -Wincompatible-pointer-types -Wint-conversion
CC = gcc

ASM=nasm
ASMFLAGS=-felf64

# Название файла без расширения, в котором лежит функция main
MAIN_FILE = main

# Папка в которой будут храниться .d файлы - файлы зависимостей.
# Они генерируются непосредственно компилятором при помощи флагов -M...
DEPDIR := .deps
CDEPFLAGS = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.d
ASMDEPFLAGS = -MT $@ -MD -MP -MF $(DEPDIR)/$*.d

LD=gcc
LDFLAGS=-no-pie

SRCDIR := src
INCLUDE := include
CFLAGS += $(shell find ${SRCDIR} -type d | sed 's/^/-I/g')
CFLAGS += $(shell find ${SRCDIR} -type d | sed 's/${SRCDIR}/-I${INCLUDE}/g')

# Парсим директорию и находим все .asm файлы, пишем их в строку через пробел
C_FILES=$(shell find ${SRCDIR}/ -name '*.c' | xargs echo | sed 's/${SRCDIR}\///g')
ASM_FILES=$(shell find ${SRCDIR}/ -name '*.asm' | xargs echo | sed 's/${SRCDIR}\///g')

OBJDIR := .build
# patsubst заменяет вхождения первого аргумента на второй
# в каждом слове третьего аргумента(слова разделены пробелом)
OBJ_FILES := $(patsubst %.c, $(OBJDIR)/%.o, $(C_FILES))
OBJ_FILES += $(patsubst %.asm, $(OBJDIR)/%.o, $(ASM_FILES))

all: $(PROG_NAME)

debug: CFLAGS += -g
debug: ASMFLAGS += -g
debug: $(PROG_NAME)

$(PROG_NAME) : $(OBJ_FILES)
	$(LD) $(LDFLAGS) -o $@ $^

$(OBJDIR)/%.o : ${SRCDIR}/%.c | ${DEPDIR} ${OBJDIR}
	$(CC) $(CDEPFLAGS) $(CFLAGS) -o $@ $<
	
$(OBJDIR)/%.o : ${SRCDIR}/%.c $(DEPDIR)/%.d | $(DEPDIR) $(OBJDIR)
	$(CC) $(CDEPFLAGS) $(CFLAGS) -o $@ $<
	
$(OBJDIR)/%.o : ${SRCDIR}/%.asm | $(DEPDIR) $(OBJDIR)
	$(ASM) $(ASMDEPFLAGS) $(ASMFLAGS) -o $@ $<

$(OBJDIR)/%.o : ${SRCDIR}/%.asm $(DEPDIR)/%.d | $(DEPDIR) $(OBJDIR)
	$(ASM) $(ASMDEPFLAGS) $(ASMFLAGS) -o $@ $<
	
$(DEPDIR) $(OBJDIR) : ; @mkdir -p $(shell find $(SRCDIR)/ -type d | sed 's/$(SRCDIR)/$@/g')

DEP_FILES = $(patsubst %.c, $(DEPDIR)/%.d, $(С_FILES))
$(DEP_FILES): $(DEPDIR)

TESTDIR := tests
TESTS_C = $(shell find $(TESTDIR)/ -name '*.c' | xargs echo | sed 's/tests\///g')
TESTS = $(subst .c,,$(TESTS_C))
TESTS_OBJ = $(patsubst %.c, $(OBJDIR)/%.o, $(TESTS_C))

# Запускаем каждый тестовый файл отдельно, линкуем его со всем кроме ${MAIN_FILE}.o
.SILENT:$(TESTS: %)
$(TESTS): % : $(OBJDIR)/%.o $(filter-out $(OBJDIR)/${MAIN_FILE}.o, $(OBJ_FILES))
	$(LD) $(LDFLAGS) -o $@ $^
	@echo "\nRunning file $@"
	@./$@
	
$(OBJDIR)/%.o : ${TESTDIR}/%.c | ${DEPDIR} ${OBJDIR}
	$(CC) $(CDEPFLAGS) $(CFLAGS) -o $@ $<
	$(POSTCOMPILE)
	
$(OBJDIR)/%.o : ${TESTDIR}/%.c $(DEPDIR)/%.d | $(DEPDIR) $(OBJDIR)
	$(CC) $(CDEPFLAGS) $(CFLAGS) -o $@ $<
	$(POSTCOMPILE)

# Создаем тестовые бинарники, запускаем и удаляем.
test: $(TESTS)
	@rm -rf $(TESTS)

.PHONY: run
run:
	@./$(PROG_NAME)

.PHONY: clean
clean:
	@rm -rf $(PROG_NAME)
	@rm -rf $(DEPDIR)
	@rm -rf $(OBJDIR)
	@rm -rf $(TESTS)

include $(shell find ${DEPDIR}/ -name '*.d' | xargs echo)