#pragma once

#ifndef IMAGE_TOOLS_H
#define IMAGE_TOOLS_H

#include "image.h"

void copy_image(struct image const *src, struct image *dst);
void free_image(struct image *img);
void create_image(struct image *dst, uint64_t width, uint64_t height);

#endif /* IMAGE_TOOLS_H */
