#pragma once

#ifndef FAST_SEPIA_H
#define FAST_SEPIA_H

struct image fast_sepia( const struct image src );

#endif /* FAST_SEPIA_H */
