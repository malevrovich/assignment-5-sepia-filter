#pragma once

#ifndef SEPIA_H
#define SEPIA_H

#include "image.h"

void sepia_one(struct pixel* restrict px);

struct image sepia( const struct image src ); 

#endif /* SEPIA_H */
