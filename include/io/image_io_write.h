#pragma once

#ifndef IMAGE_IO_WRITE_H
#define IMAGE_IO_WRITE_H

#include <stdio.h>

#include "image.h"
#include "io/types/image_type.h"

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    
    WRITE_CONTENT_WRONG,
    WRITE_NULL_FILE_POINTER,

    WRITE_NO_ACCESS,
    WRITE_NULL_PATH,
    WRITE_BAD_DESCRIPTOR,
    WRITE_IS_DIR,
    WRITE_FILE_ERROR,

    WRITE_UNSUPPORTED_TYPE,
    WRITE_UNDEFINED_TYPE
};

char * write_status_to_str(const enum write_status status);

enum write_status write_image_to(const char* const path, const struct image* const img);
enum write_status write_explicit_image_to(const char* const path,
                                            const enum image_type type, const struct image* const img);
enum write_status write_image(FILE * const out, const enum image_type type, const struct image* const img);

#endif /* IMAGE_IO_WRITE_H */
