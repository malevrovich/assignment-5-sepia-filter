#pragma once

#ifndef IMAGE_IO_READ_H
#define IMAGE_IO_READ_H

#include <stdio.h>

#include "image.h"
#include "io/types/image_type.h"

enum read_status {
    READ_OK = 0,
    
    READ_UNSUPPORTED_TYPE,
    READ_UNDEFINED_TYPE,

    READ_WRONG_CONTENT,
    READ_NULL_FILE_POINTER,

    READ_NO_ACCESS,
    READ_NULL_PATH,
    READ_BAD_DESCRIPTOR,
    READ_FILE_BIG,
    READ_IS_DIR,
    READ_FILE_NOT_FOUND,
    
    READ_FILE_ERROR
};

char * read_status_to_str(const enum read_status status);

enum read_status read_image_from(const char * const path, struct image* const img);
enum read_status read_explicit_image_from(const char * const path, const enum image_type type, struct image* const img);
enum read_status read_image(FILE * const in, const enum image_type type, struct image * const img);

#endif /* IMAGE_IO_READ_H */
