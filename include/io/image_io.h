#pragma once

#ifndef IMAGE_IO_H
#define IMAGE_IO_H

#include "image.h"

#include "io/image_io_read.h"
#include "io/image_io_write.h"

#endif /* IMAGE_IO_H */
