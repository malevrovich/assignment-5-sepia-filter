#pragma once

#ifndef IMAGE_TYPE_H
#define IMAGE_TYPE_H

enum image_type {
    BMP
};

int ext_to_type(const char* const ext, enum image_type* const dst);

#endif /* IMAGE_TYPE_H */
