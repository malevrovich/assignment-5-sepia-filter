#pragma once

#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdio.h>

enum bmp_read_status {
    BMP_READ_OK = 0,
    BMP_READ_INVALID_SIGNATURE,
    BMP_READ_UNSUPPORTED_TYPE,
    
    BMP_READ_NULL_IMAGE,

    BMP_READ_INVALID_HEADER,
    BMP_READ_INVALID_BITS,
    BMP_READ_MEMORY_EXCEED
};

char * bmp_read_status_to_str(const enum bmp_read_status status);

enum bmp_write_status {
    BMP_WRITE_OK = 0,

    BMP_WRITE_NULL_IMAGE,
    BMP_WRITE_TOO_BIG,

    BMP_WRITE_IO_ERROR
};

char * bmp_write_status_to_str(const enum bmp_write_status status);

enum bmp_read_status from_bmp( FILE * const in, struct image * const img );

enum bmp_write_status to_bmp( FILE * const out, const struct image * const img );

#endif /* BMP_H */
